numresults = 10

from bs4 import BeautifulSoup
import urllib
import os
import re

proxybay = BeautifulSoup(urllib.urlopen('http://proxybay.info/'))
proxybay_tr = proxybay.find_all('tr')[1:]
for tr in proxybay_tr:
    tr_td = tr.find_all('td')
    if tr_td[2].img['alt'] == 'up':
        url = tr_td[0].a['href']
        break

query = raw_input('Enter search term: ').split()
query = '%20'.join(query)

page = BeautifulSoup(urllib.urlopen(url + '/search/' + query))
#find titles and links of each result
results = page.find_all('div',{'class':'detName'})
if len(results) < numresults:
    numresults = len(results)
results = results[0:numresults]
titles = [result.a.get_text() for result in results]
links = [result.a['href'] for result in results]
#find upload date and size of each result
resultdesc_html = page.find_all('font',{'class':'detDesc'})[0:numresults]
resultdesc = [result.get_text() for result in resultdesc_html]
dates, sizes = [], []
for result in resultdesc:
    date = re.search("Uploaded (\S+),",result)
    dates.append(date.groups()[0])
    size = re.search(", Size (\S+),",result)
    sizes.append(size.groups()[0])
#find SE and LE for each result
SELE_html = page.find_all('td',{'align':'right'})[0:numresults*2]
SELE = [tag.get_text() for tag in SELE_html]
SE = SELE[0::2]
LE = SELE[1::2]

another = True
downloading = []

while another:

    for resultID in range(numresults):
        if resultID in downloading:
            DLstr = '| DOWNLOAD IN PROGRESS'
        else:
            DLstr = ''
        strtuple = (resultID,titles[resultID],sizes[resultID],SE[resultID],LE[resultID],dates[resultID],DLstr)
        prntstr = '%i. %s | Size: %s | Seeds: %s | Leechers: %s | Date: %s %s' % strtuple
        print prntstr

    resultID = int(raw_input('Which one do you want? '))
    downloading.append(resultID)
    print 'Downloading .torrent file...'
    desired = links[resultID].split('/')

    desiredID, name = desired[2], desired[3]

    DLurl = url.replace('//','//torrents.',1) + desiredID + '/' + name + '.torrent'

    urllib.urlretrieve(DLurl, 'D:\\Downloads\\Bittorrent Downloads\\Torrents\\' + name + '.torrent')

    print 'Starting torrent client...'
    os.system('start D:/Downloads/"Bittorrent Downloads"/Torrents/' + name + '.torrent')

    finished = raw_input('Finished? (y/n) ')
    if finished == 'y':
        another = False
    elif finished == 'n':
        another = True
    else:
        another = False
        print 'Input not recognised, closing...'
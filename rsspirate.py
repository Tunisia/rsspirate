# windows only

import feedparser as fp
import fnmatch as fnm
from datetime import date, timedelta, datetime
from bs4 import BeautifulSoup
import urllib
import os
import string

def sanitise(badfile):
    valid_chars = "-_.()%s%s" % (string.ascii_letters, string.digits)
    return ''.join(c for c in badfile if c in valid_chars)

with open('D:\\Downloads\\Bittorrent Downloads\\Torrents\\lastrun.txt','r') as f:
    lastrun = datetime.strptime(f.read(),"%Y-%m-%d").date()

feed = fp.parse("http://followshows.com/feed/iawwCEXQ")

entries = sorted(feed['items'], key=lambda entry: entry["date_parsed"])
entries.reverse()

#yesterday's TV

tDate = date.today().isoformat()
tm = date.today() + timedelta(1)
tomDate = tm.isoformat()

#choose a torrent site

proxybay = BeautifulSoup(urllib.urlopen('http://proxybay.info/'))
proxybay_tr = proxybay.find_all('tr')[1:]
#finds site with fastest ping- not necessarily fastest search result!
#sitelist = []
#speedlist = []
#for tr in proxybay_tr:
#    tr_td = tr.find_all('td')
#    if tr_td[2].img['alt'] == 'up':
#        sitelist.append(tr_td[0].a['href'])
#        speedlist.append(float(tr_td[-1].string))
#speedsite = sorted(zip(speedlist,sitelist))
#url = speedsite[0][1]
for tr in proxybay_tr:
    tr_td = tr.find_all('td')
    if tr_td[2].img['alt'] == 'up':
        url = tr_td[0].a['href']
        break

queries = []

for entry in entries:
    if entry['date'][0:10] == tomDate:
        continue
    if datetime.strptime(entry['date'],"%Y-%m-%dT%H:%M:%SZ").date() <= lastrun:
        break

    title = entry['title'].split(' ')
    filename = sanitise('_'.join(title)) + '.torrent'

    SE = fnm.filter(title,'S??E??')[0]
    query = [SE]

    for word in title:
        if word == SE:
            break
        else:
            query.append(word)

    query.append(query.pop(0))
    query = '%20'.join(query)

    results = BeautifulSoup(urllib.urlopen(url + '/search/' + query))
    firstresult = results.find('div',{'class':'detName'}).a['href']
    firstresult = firstresult.split('/')
        #consider redundancies for if first result is bum?
    FRnum, FRname = firstresult[2], firstresult[3]

    DLurl = url.split('//')
    DLurl = '//torrents.'.join(DLurl) + FRnum + '/' + FRname + '.torrent'

    urllib.urlretrieve(DLurl, 'D:\\Downloads\\Bittorrent Downloads\\Torrents\\' + filename)

    try:
        os.startfile('D:\\Downloads\\Bittorrent Downloads\\Torrents\\' + filename)
    except Exception as exc:
        with open('D:\\Downloads\\Bittorrent Downloads\\Torrents\\errors.txt','a') as errors:
            errors.write('\n' + str(exc))

with open('D:\\Downloads\\Bittorrent Downloads\\Torrents\\lastrun.txt','w') as f:
    f.write(tDate)